## Features

This software has following features:
- Roles: Master, Admin, Teacher, Student, Librarian, Accountant
- Attendance
- Mark
- Registration
- Notice, Syllabus
- Library
- Exam
- Grade
- Accounts
- Messaging (uses CKEditor 5)
