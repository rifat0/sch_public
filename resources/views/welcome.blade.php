<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Ultimate School Management System</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    School Managment Template
                </div>

                <div class="links">
                    <a href="https://themeforest.net/item/kids-life-a-trendy-kids-html-template/8565477?s_rank=3">Kids Life</a>
                    <a href="https://themeforest.net/item/superowl-kindergarten-school-of-early-learning-nanny-agency-html-template/19567129?_ga=2.217223700.367138064.1554784894-450163697.1554784894">SuperOwl</a>
                </div>
                <h2>
                    <h1>At a high level, the main features of Unified Transform include the following:</h1>

                        <h2>Roles:-  Master, Admin, Teacher, Student, Librarian, Accountant</h2>
                        <ol>
                            <h3><li>Attendance</li></h3>
                            <h3><li>Mark</li></h3>
                            <h3><li>Registration</li></h3>
                            <h3><li>Notice, Syllabus</li></h3>
                            <h3><li>Library</li></h3>
                            <h3><li>Exam</li></h3>
                            <h3><li>Grade</li></h3>
                            <h3><li>Accounts</li></h3>
                            <h3><li>Messaging</li></h3>
                        </ol>

                    </h2>
            </div>
        </div>
    </body>
</html>
